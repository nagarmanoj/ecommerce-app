$(document).ready(function(){
	// Check admin password
	$("#current_password").keyup(function(){
		//alert(current_password);
		var current_password = $("#current_password").val();
		$.ajax({
			type:'post',
			url:'/admin/check-admin-password',
			headers:{
				'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
			},
			data:{current_password:current_password},
			success:function(resp) {
				if(resp=="false"){
					$("#check_password").html("<font color='red'>Current Password is Incorrect !</font>");
				}else if(resp=="true"){
					$("#check_password").html("<font color='green'>Current Password is Correct !</font>");
				}
			},error:function(){
				alert("Error");
			}
		})

	})
});