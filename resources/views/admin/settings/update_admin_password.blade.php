@extends('admin.layout.layout')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="row">
                    <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                        <h3 class="font-weight-bold">Settings</h3>
                        
                    </div>
                    <div class="col-12 col-xl-4">
                        <div class="justify-content-end d-flex">
                            <div class="dropdown flex-md-grow-1 flex-xl-grow-0">
                                <button class="btn btn-sm btn-light bg-white dropdown-toggle" type="button" id="dropdownMenuDate2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="mdi mdi-calendar"></i> Today (10 Jan 2021)
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDate2">
                                    <a class="dropdown-item" href="#">January - March</a>
                                    <a class="dropdown-item" href="#">March - June</a>
                                    <a class="dropdown-item" href="#">June - August</a>
                                    <a class="dropdown-item" href="#">August - November</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="col-md-6 grid-margin stretch-card">
	              <div class="card">
	                <div class="card-body">
	                  <h4 class="card-title">Update Admin Password</h4>
	                  @if(Session::has('error_message'))
		              <div class="alert alert-danger alert-dismissible fade show" role="alert">
		                <strong>Error</strong> {{Session::get('error_message')}}.
		                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                  <span aria-hidden="true">&times;</span>
		                </button>
		              </div>
		              @endif
		              @if(Session::has('success_message'))
		              <div class="alert alert-success alert-dismissible fade show" role="alert">
		                <strong>Error</strong> {{Session::get('success_message')}}.
		                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                  <span aria-hidden="true">&times;</span>
		                </button>
		              </div>
		              @endif
	                  <form class="forms-sample" action="{{ url('admin/update-admin-password')}}" method="post" >
	                  	@csrf
	                    <div class="form-group">
	                      <label for="exampleInputUsername1">Email</label>
	                      <input class="form-control" value="{{ $admindetail['email']}}" readonly="">
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputEmail1">Admin Type</label>
	                      <input class="form-control" value="{{ $admindetail['type']}}" readonly="">
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">Current Password</label>
	                      <input type="password" name="current_password" id="current_password" class="form-control" id="exampleInputPassword1" placeholder="Current Password" required="">
	                      <span id="check_password"></span>
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">New Password</label>
	                      <input type="password" name="new_password" id="new_password" class="form-control" id="exampleInputPassword1" placeholder="New Password" required="">
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputConfirmPassword1">Confirm New Password</label>
	                      <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control" id="exampleInputConfirmPassword1" placeholder="Confirm New Password" required="">
	                    </div>
	                    <div class="form-check form-check-flat form-check-primary">
	                      <label class="form-check-label">
	                        <input type="checkbox" class="form-check-input">
	                        Remember me
	                      </label>
	                    </div>
	                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
	                    <button class="btn btn-light">Cancel</button>
	                  </form>
	                </div>
	              </div>
	            </div>
			</div>
	</div>
</div>
@endsection