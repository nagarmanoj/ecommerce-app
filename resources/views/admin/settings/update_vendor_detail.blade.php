@extends('admin.layout.layout')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="row">
                    <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                        <h3 class="font-weight-bold">Update Vendor Details</h3>
                        
                    </div>
                    <div class="col-12 col-xl-4">
                        <div class="justify-content-end d-flex">
                            <div class="dropdown flex-md-grow-1 flex-xl-grow-0">
                                <button class="btn btn-sm btn-light bg-white dropdown-toggle" type="button" id="dropdownMenuDate2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="mdi mdi-calendar"></i> Today (10 Jan 2021)
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDate2">
                                    <a class="dropdown-item" href="#">January - March</a>
                                    <a class="dropdown-item" href="#">March - June</a>
                                    <a class="dropdown-item" href="#">June - August</a>
                                    <a class="dropdown-item" href="#">August - November</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($slug=="personal")
		<div class="row">
			<div class="col-md-6 grid-margin stretch-card">
	              <div class="card">
	                <div class="card-body">
	                  <h4 class="card-title">Update Personal Infornation</h4>
	                  @if(Session::has('error_message'))
		              <div class="alert alert-danger alert-dismissible fade show" role="alert">
		                <strong>Error</strong> {{Session::get('error_message')}}.
		                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                  <span aria-hidden="true">&times;</span>
		                </button>
		              </div>
		              @endif
		              @if(Session::has('success_message'))
		              <div class="alert alert-success alert-dismissible fade show" role="alert">
		                <strong>Success</strong> {{Session::get('success_message')}}.
		                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                  <span aria-hidden="true">&times;</span>
		                </button>
		              </div>
		              @endif
		              @if($errors->any())
		                <div class="alert alert-danger">
		                    <ul>
		                      @foreach($errors->all() as $error)
		                        <li>{{$error}}</li>
		                      @endforeach
		                    </ul>
		                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
		                      @foreach($errors->all() as $error)
		                        <li>{{$error}}</li>
		                      @endforeach
		                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                        <span aria-hidden="true">&times;</span>
		                      </button>
		                    </div>
		                </div>
		              @endif
	                  <form class="forms-sample" action="{{ url('admin/update-admin-details')}}" method="post"  enctype="multipart/form-data">
	                  	@csrf
	                    <div class="form-group">
	                      <label for="exampleInputUsername1">Vendor Email</label>
	                      <input class="form-control" value="{{ Auth::guard('admin')->user()->email }}" readonly="">
	                    </div>
	                    
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">Name</label>
	                      <input type="text" name="vendor_name" id="vendor_name" value="{{ Auth::guard('admin')->user()->name }}" class="form-control" id="exampleInputPassword1" placeholder="Name" required="">                      
	                    </div>
	                    
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">Address</label>
	                      <input type="text" name="vendor_address" id="vendor_address" value="{{ $vendordetails['address']}}" class="form-control" id="exampleInputPassword1" placeholder="Address" required="">                      
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">City</label>
	                      <input type="text" name="vendor_city" id="vendor_city" value="{{ $vendordetails['city'] }}" class="form-control" id="exampleInputPassword1" placeholder="City" required="">                      
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">State</label>
	                      <input type="text" name="vendor_state" id="vendor_state" value="{{ $vendordetails['state'] }}" class="form-control" id="exampleInputPassword1" placeholder="State" required="">                      
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">Country</label>
	                      <input type="text" name="vendor_country" id="vendor_country" value="{{ $vendordetails['country'] }}" class="form-control" id="exampleInputPassword1" placeholder="Country" required="">                      
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">Pincode</label>
	                      <input type="text" name="vendor_pincode" id="vendor_pincode" value="{{ $vendordetails['pincode'] }}" class="form-control" id="exampleInputPassword1" placeholder="Pincode" required="">                      
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">Name</label>
	                      <input type="text" name="admin_name" id="admin_name" value="{{ Auth::guard('admin')->user()->name }}" class="form-control" id="exampleInputPassword1" placeholder="Name" required="">                      
	                    </div>
	                    <div class="form-group">
	                      <label for="exampleInputPassword1">Mobile</label>
	                      <input type="text" name="admin_mobile" value="{{ Auth::guard('admin')->user()->mobile }}" id="admin_mobile" class="form-control" id="exampleInputPassword1" placeholder="Enter 10 Digit Mobile Number" required="" maxlength="10" minlength="10">
	                    </div>
	                    <div class="form-group">
	                      <label for="admin_image">Photo</label>
	                      <input type="file" name="admin_image"  id="admin_image" class="form-control" id="exampleInputPassword1"  required="">
	                      @if(!empty(Auth::guard('admin')->user()->image))
	                      	<a href="{{ url('admin/uploads/photos/'.Auth::guard('admin')->user()->image)}}">View Image</a>
	                      	<input type="hidden" name="current_admin_image" value="{{
	                      		Auth::guard('admin')->user()->image
	                      	}}">
	                      @endif
	                    </div>
	                    <div class="form-check form-check-flat form-check-primary">
	                      <label class="form-check-label">
	                        <input type="checkbox" class="form-check-input">
	                        Remember me
	                      </label>
	                    </div>
	                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
	                    <button class="btn btn-light">Cancel</button>
	                  </form>
	                </div>
	              </div>
	            </div>
			</div>
		@elseif($slug=="business")

		@elseif($slug=="bank")

		@endif
	</div>
</div>
@endsection