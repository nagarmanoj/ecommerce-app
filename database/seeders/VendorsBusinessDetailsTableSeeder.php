<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\VendorsBusinessDetail;

class VendorsBusinessDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $vendorRecords = [
            ['id'=>1,'vendor_id'=>1,'shop_name'=>'Suso Electronics Store','shop_address'=>'1234-SCF','shop_city'=>'Jaipur','shop_state'=>'Rajasthan','shop_country'=>'India','shop_pincode'=>'302033','shop_mobile'=>'7894561230','shop_website'=>'sudoelectronics.in','shop_email'=>'sudo@sudo.com','address_proof'=>'Passport','address_proof_image'=>'test.jpg','business_license_number'=>'1312456987','gst_number'=>'789654','pan_number'=>'258963'],
        ];

        VendorsBusinessDetail::insert($vendorRecords);
    }
}
