<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $adminRecords = [
            ['id'=>2,
            'name'=>'sudo',
            'type'=>'vendor',
            'vendor_id'=>1,
            'mobile'=>'7894561230',
            'email'=>'sudo@sudo.com',
            'password'=>'$2y$10$iJarqd8ri2himre5WlgX/OdyyYzW6TQuKekA3pRBq5Gucyr2XOJSu',
            'image'=>'abc.jpg',
            'status'=>0]
        ];
        Admin::insert($adminRecords);
    }
}
