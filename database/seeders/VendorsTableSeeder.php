<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Vendor;
class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $vendorRecords = [
            ['id'=>1,'name'=>'sudo','address'=>'CP-112','city'=>'jaipur','state'=>'Rajasthan','country'=>'India','pincode'=>'302033','mobile'=>'7894561230','email'=>'sudo@gmail.com','status'=>0],
        ];
        Vendor::insert($vendorRecords);
    }
}
