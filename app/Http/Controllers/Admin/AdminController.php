<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Hash;
use Image;
use App\Models\Admin;
use App\Models\Vendor;
class AdminController extends Controller
{
    //

    public function dashboard(){
        return view('admin.dashboard');
    }

    public function login(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $rule = [
                'email'=>'required|email|max:255',
                'password'=>'required',
            ];

            $customMessage = [
                'email.required' => 'Email Address is required',
                'email.email' => 'Valid Email Address is required',
                'email.required' => 'Password is required',
            ];

            $this->validate($request,$rule,$customMessage);

            if(Auth::guard('admin')->attempt(['email'=>$data['email'],'password'=>$data['password'],'status'=>1])){
                return redirect('admin/dashboard');
            }else{
                return redirect()->back()->with('error_message','Invalid Email or Password');
            }
        }
        return view('admin.login');
    }

    public function logout(){
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

    public function updateAdminPassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            if(Hash::check($data['current_password'],Auth::guard('admin')->user()->password)){
                //check if new password is matching with confirm password 
                if($data['confirm_new_password']==$data['new_password']){
                    Admin::where('id',Auth::guard('admin')->user()->id)->update(['password'=>bcrypt($data['new_password'])]);
                    return redirect()->back()->with('success_message','Password has been updated successfylly');
                }else{
                    return redirect()->back()->with('error_message','New Password and Confirm Password does not match !');
                }
            }else{
                return redirect()->back()->with('error_message','Your current password is Incorrect !');
            }
        }
        $admindetail = Admin::where('email',Auth::guard('admin')->user()->email)->first()->toArray();
        return view('admin.settings.update_admin_password')->with(compact('admindetail'));
    }

    public function checkAdminPassword(Request $request){
        $data = $request->all();
        // echo "<pre>"; print_r($data); die;
        if(Hash::check($data['current_password'],Auth::guard('admin')->user()->password)){
            return "true";
        }else{
            return "false";
        }
    }


    //Admin detail
    public function updateAdminDetails(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $rule = [
                'admin_name'=>'required|regex:/^[\pL\s\-]+$/u',
                'admin_mobile'=>'required|numeric',
            ];

            $this->validate($request,$rule);
            //upload admin photo 
            if($request->hasFile('admin_image')){
                $image_tmp = $request->file('admin_image');
                if($image_tmp->isValid()){
                    //get image extension 
                    $extension = $image_tmp->getClientOriginalExtension();
                    //generate_image name 
                    $imageName = rand(111,99999).'.'.$extension;
                    $imagePath = 'admin/uploads/photos/'.$imageName;
                    Image::make($image_tmp)->save($imagePath);
                }
            }else if(!empty($data['current_admin_image'])){
                $imageName = $data['current_admin_image'];
            }else{
                $imageName = "";
            }
            Admin::where('id',Auth::guard('admin')->user()->id)->update(['name'=>$data['admin_name'],'mobile'=>$data['admin_mobile'],'image'=>$imageName]);
            return redirect()->back()->with('success_message','Admin details updated successfylly !');
        }
        return view('admin.settings.update_admin_details');
    }

    //vendor Details

    public function updateVendorDetails($slug){
        if($slug=="personal"){
            $vendordetails = Vendor::where('id',Auth::guard('admin')->user()->vendor_id)->first()->toArray();

        }else if($slug=="business"){

        }else if($slug=="bank"){

        }
        return view('admin.settings.update_vendor_detail')->with(compact('slug','vendordetails'));
    }
}
